require('dotenv').config

module.exports = {
    "type": "postgres",
    "url": process.env.DATABASE_URL,
    "host": "localhost",
    "port": process.env.PORT,
    "username": "postgres",
    "password": "admin",
    "database": "todo-backend1",
    "synchronize": true,
    "ssl": true,
    "extra": {
      "ssl": {
        "rejectUnauthorized": false
      }
    },
    "logging": "all",
    "entities": [
       "src/entity/**/*.ts"
    ],
    "migrations": [
       "src/migration/**/*.ts"
    ],
    "subscribers": [
       "src/subscriber/**/*.ts"
    ],
    "cli": {
       "entitiesDir": "src/entity",
       "migrationsDir": "src/migration",
       "subscribersDir": "src/subscriber"
    }
}