import {MigrationInterface, QueryRunner} from "typeorm";

export class dropUserTable1614732713610 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE public."user" (
        id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
        "firstName" character varying COLLATE pg_catalog."default" NOT NULL,
        "lastName" character varying COLLATE pg_catalog."default" NOT NULL,
        age integer NOT NULL,
        CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id))`);
    }

}
