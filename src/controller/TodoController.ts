import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Todo} from "../entity/Todo";

export class TodoController {

    private todoRepository = getRepository(Todo);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.todoRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.todoRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.todoRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            const todoToRemove = await this.todoRepository.findOne({ frontEndId: request.body.frontEndId });
            await this.todoRepository.remove(todoToRemove);
            return todoToRemove;
        }
        catch(err) {
            console.log(err)
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        try {
            await this.todoRepository.update(request.params.id, request.body)
            const updatedTodo = await this.todoRepository.findOne({ frontEndId: request.body.frontEndId });
            return updatedTodo;
        }
        catch(err) {
            console.log(err)
        }
    }

}