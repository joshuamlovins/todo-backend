import {TodoController} from "./controller/TodoController";

export const Routes = [{
    method: "get",
    route: "/todos",
    controller: TodoController,
    action: "all"
}, {
    method: "post",
    route: "/todos",
    controller: TodoController,
    action: "save"
}, {    
    method: "delete",
    route: "/todos",
    controller: TodoController,
    action: "remove"
}, {
    method: "put",
    route: "/todos",
    controller: TodoController,
    action: "update"
}];